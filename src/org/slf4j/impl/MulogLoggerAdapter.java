/**
 * Copyright (c) 2004-2011 QOS.ch
 * All rights reserved.
 *
 * Permission is hereby granted, free  of charge, to any person obtaining
 * a  copy  of this  software  and  associated  documentation files  (the
 * "Software"), to  deal in  the Software without  restriction, including
 * without limitation  the rights to  use, copy, modify,  merge, publish,
 * distribute,  sublicense, and/or sell  copies of  the Software,  and to
 * permit persons to whom the Software  is furnished to do so, subject to
 * the following conditions:
 *
 * The  above  copyright  notice  and  this permission  notice  shall  be
 * included in all copies or substantial portions of the Software.
 *
 * THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
 * EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
 * MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package org.slf4j.impl;
import org.slf4j.impl.Mulog;
import org.slf4j.event.Level;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MarkerIgnoringBase;
import org.slf4j.helpers.MessageFormatter;
import org.slf4j.helpers.Util;
public final class MulogLoggerAdapter extends MarkerIgnoringBase {
    private static final long serialVersionUID = 4141593414490482208L;
    private String nm;
    MulogLoggerAdapter(String nm){this.nm=nm;}
    public boolean isTraceEnabled() {return Mulog.isAtLeast(Level.TRACE);}
    public boolean isDebugEnabled() {return Mulog.isAtLeast(Level.DEBUG);}
    public boolean isInfoEnabled() {return Mulog.isAtLeast(Level.INFO);}
    public boolean isWarnEnabled() {return Mulog.isAtLeast(Level.WARN);}
    public boolean isErrorEnabled() {return Mulog.isAtLeast(Level.ERROR);}
    public void trace(String msg) {if(isTraceEnabled())Mulog.trace(nm,msg);}
    public void debug(String msg) {if(isDebugEnabled())Mulog.debug(nm,msg);}
    public void info (String msg) {if(isInfoEnabled())Mulog.info(nm,msg);}
    public void warn (String msg) {if(isWarnEnabled())Mulog.warn(nm,msg);}
    public void error(String msg) {if(isErrorEnabled())Mulog.error(nm,msg);}
    public void trace(String msg,Throwable t) {if(isTraceEnabled())Mulog.trace(nm,msg,t);}
    public void debug(String msg,Throwable t) {if(isDebugEnabled())Mulog.debug(nm,msg,t);}
    public void info (String msg,Throwable t) {if(isInfoEnabled())Mulog.info(nm,msg,t);}
    public void warn (String msg,Throwable t) {if(isWarnEnabled())Mulog.warn(nm,msg,t);}
    public void error(String msg,Throwable t) {if(isErrorEnabled())Mulog.error(nm,msg,t);}
    private String fmt(String fmt,Object a){return MessageFormatter.format(fmt,a).getMessage();}
    private String fmt(String fmt,Object a, Object b){return MessageFormatter.format(fmt,a,b).getMessage();}
    private String afmt(String fmt,Object[] as){return MessageFormatter.arrayFormat(fmt,as).getMessage();}
    public void trace(String format,Object arg){if(isTraceEnabled())Mulog.trace(nm,fmt(format,arg));}
    public void trace(String format,Object a1,Object a2){if(isTraceEnabled())Mulog.trace(nm,fmt(format,a1,a2));}
    public void trace(String format,Object... arguments){if(isTraceEnabled())Mulog.trace(nm,afmt(format,arguments));}
    public void debug(String format,Object arg){if(isDebugEnabled())Mulog.debug(nm,fmt(format,arg));}
    public void debug(String format,Object a1,Object a2){if(isDebugEnabled())Mulog.debug(nm,fmt(format,a1,a2));}
    public void debug(String format,Object... arguments){if(isDebugEnabled())Mulog.debug(nm,afmt(format,arguments));}
    public void info(String format,Object arg){if(isInfoEnabled())Mulog.info(nm,fmt(format,arg));}
    public void info(String format,Object a1,Object a2){if(isInfoEnabled())Mulog.info(nm,fmt(format,a1,a2));}
    public void info(String format,Object... arguments){if(isInfoEnabled())Mulog.info(nm,afmt(format,arguments));}
    public void warn(String format,Object arg){if(isWarnEnabled())Mulog.warn(nm,fmt(format,arg));}
    public void warn(String format,Object a1,Object a2){if(isWarnEnabled())Mulog.warn(nm,fmt(format,a1,a2));}
    public void warn(String format,Object... arguments){if(isWarnEnabled())Mulog.warn(nm,afmt(format,arguments));}
    public void error(String format,Object arg){if(isErrorEnabled())Mulog.error(nm,fmt(format,arg));}
    public void error(String format,Object a1,Object a2){if(isErrorEnabled())Mulog.error(nm,fmt(format,a1,a2));}
    public void error(String format,Object... arguments){if(isErrorEnabled())Mulog.error(nm,afmt(format,arguments));}
}
