(ns org.slf4j.impl.mulog
  (:require [com.brunobonacci.mulog :as u])
  (:import [org.slf4j.event Level]
           [org.slf4j.impl Mulog]))
(defn log
  ([cls lvl msg]    (u/log :log/message :mulog/namespace cls :mulog/origin :slf4j-mulog
                           :log/message msg :log/level (keyword lvl)))
  ([cls lvl msg ex] (u/log :log/message :mulog/namespace cls :mulog/origin :slf4j-mulog
                           :log/message msg :log/level (keyword lvl) :exception ex)))
(def log-level {:trace Level/TRACE :debug Level/DEBUG :info Level/INFO :warn Level/WARN :error Level/ERROR})
(def level-log {Level/TRACE :trace Level/DEBUG :debug Level/INFO :info Level/WARN :warn Level/ERROR :error})
(defn level [] (level-log (Mulog/level)))
(defn set-level! [kw] (if-some [lv (log-level kw)] (Mulog/level lv) (throw (ex-info "no such log level" {:cause ::bad-level :value kw}))))

(comment
  (u/start-publisher! {:type :console})
  (def logger (org.slf4j.LoggerFactory/getLogger java.util.Map))
  (level)
  (set-level! :debug2)
  (.info logger "foo{}" "bar")
  (.info logger "foo{}" (Throwable.))
  )
