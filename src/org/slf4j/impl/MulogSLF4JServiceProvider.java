package org.slf4j.impl;

import org.slf4j.spi.SLF4JServiceProvider;
import org.slf4j.ILoggerFactory;
import org.slf4j.IMarkerFactory;
import org.slf4j.helpers.BasicMarkerFactory;
import org.slf4j.spi.MDCAdapter;
import org.slf4j.helpers.NOPMDCAdapter;

public class MulogSLF4JServiceProvider implements SLF4JServiceProvider {

    private final ILoggerFactory loggerFactory;
    private final IMarkerFactory markerFactory = new BasicMarkerFactory();

    public MulogSLF4JServiceProvider() {
        loggerFactory = new MulogLoggerFactory();
    }

    @Override
    public ILoggerFactory getLoggerFactory() {
        return loggerFactory;
    }

    @Override
    public IMarkerFactory getMarkerFactory() {
        return markerFactory;
    }

    @Override
    public MDCAdapter getMDCAdapter() {
        return new NOPMDCAdapter();
    }

    @Override
    public String getRequestedApiVersion() {
        return "2.0.16";
    }

    @Override
    public void initialize() {
        return;
    }

}
