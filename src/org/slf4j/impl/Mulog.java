package org.slf4j.impl;
import org.slf4j.event.Level;
import clojure.java.api.Clojure;
import clojure.lang.IFn;
public final class Mulog {
    private static IFn log;
    static{
        IFn rq = Clojure.var("clojure.core","require");
        rq.invoke(Clojure.read("org.slf4j.impl.mulog"));
        log=Clojure.var("org.slf4j.impl.mulog","log");}
    private static Level lvl = Level.INFO;
    public static Level level(){return lvl;}
    public static Level level(Level nw){lvl=nw;return nw;}
    public static boolean isAtLeast(Level l){return l.compareTo(lvl)<=0;}
    public static void trace(String nm,String msg){log.invoke(nm,"trace",msg);}
    public static void debug(String nm,String msg){log.invoke(nm,"debug",msg);}
    public static void info (String nm,String msg){log.invoke(nm,"info" ,msg);}
    public static void warn (String nm,String msg){log.invoke(nm,"warn" ,msg);}
    public static void error(String nm,String msg){log.invoke(nm,"error",msg);}
    public static void trace(String nm,String msg,Throwable t){log.invoke(nm,"trace",msg,t);}
    public static void debug(String nm,String msg,Throwable t){log.invoke(nm,"debug",msg,t);}
    public static void info (String nm,String msg,Throwable t){log.invoke(nm,"info" ,msg,t);}
    public static void warn (String nm,String msg,Throwable t){log.invoke(nm,"warn" ,msg,t);}
    public static void error(String nm,String msg,Throwable t){log.invoke(nm,"error",msg,t);}
}
