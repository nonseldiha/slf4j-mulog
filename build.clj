(ns build
  "Build script.

  Clean first:
  clojure -T:build clean

  Then, compile and create jar:
  clojure -T:build compile
  clojure -T:build jar

  Finally, deploy to clojars or install into local .m2 repository:
  clojure -T:build deploy
  clojure -T:build install

  For more information, run:
  clojure -A:deps -T:build help/doc"
  (:refer-clojure :exclude [compile test])
  (:require
    [clojure.tools.build.api :as b]
    [deps-deploy.deps-deploy :as dd]))

(def lib 'nonseldiha/slf4j-mulog)
(def version "0.3.0")
(def class-dir "target/classes")
(def meta-inf-services-dir (str class-dir "/META-INF/services"))

(defn- pom-template [version]
  [[:description "SLF4J bindings for mulog"]
   [:url "https://gitlab.com/nonseldiha/slf4j-mulog"]
   [:licenses
    [:license
     [:name "MIT License"]
     [:url "https://opensource.org/license/mit"]]]
   [:developers
    [:developer
     [:name "Peter Nagy"]]]
   [:scm
    [:url "https://gitlab.com/nonseldiha/slf4j-mulog"]
    [:connection "scm:git:https://gitlab.com/nonseldiha/slf4j-mulog.git"]
    [:developerConnection "scm:git:ssh:git@gitlab.com:nonseldiha/slf4j-mulog.git"]
    [:tag (str "v" version)]]])

(defn- base-opts
  [opts]
  (merge opts
         {:basis (b/create-basis {})
          :class-dir class-dir
          :jar-file  (format "target/%s-%s.jar" lib version)
          :lib lib
          :pom-data (pom-template version)
          :version version}))

(defn clean
  "Clean up."
  [_opts]
  (b/delete {:path "target"}))

(defn compile
  "Compile Java classes."
  [opts]
  (let [{:keys [class-dir basis]} (base-opts opts)]
    (b/javac {:src-dirs ["src"]
              :class-dir class-dir
              :basis basis
              :javac-opts ["--release" "11"]})))

(defn jar
  "Create jar."
  [opts]
  (let [opts (base-opts opts)]
    (b/write-pom opts)
    (b/copy-dir {:src-dirs ["src"]
                 :include "**/*.clj"
                 :target-dir class-dir})
    (b/copy-dir {:src-dirs ["build/services"]
                 :target-dir meta-inf-services-dir})
    (b/jar opts)))

(defn- deploy-opts
  [location opts]
  {:installer location
   :artifact (b/resolve-path (:jar-file opts))
   :pom-file (b/pom-path (select-keys opts [:lib :class-dir]))})

(defn deploy
  "Deploy to clojars. Requires CLOJARS_PASSWORD and CLOJARS_USERNAME."
  [opts]
  (dd/deploy (deploy-opts :remote (base-opts opts))))

(defn install
  "Install into local .m2 repository."
  [opts]
  (dd/deploy (deploy-opts :local (base-opts opts))))

(comment
  (clean nil)
  (compile nil)
  (jar nil)
  (install nil)
  (deploy nil)

  (deploy-opts :remote (base-opts nil))
  (deploy-opts :local (base-opts nil))
  )
