* Slf4j-mulog

[[https://img.shields.io/clojars/v/nonseldiha/slf4j-mulog.svg]]

A simple [[http:http://www.slf4j.org/][SLF4J]] binding for [[https://github.com/BrunoBonacci/mulog][mulog]].

** Why?

Before adopting this solution please consider the pros and cons of it.

*** Pros
- Unified logging. Configure mulog, add this library and you're done.
- Possibility of inline transformation of log lines. Just write some clojure
  code to decompose a log line into structured data instead of writing it in
  logstash's grok or other tools.

*** Cons
- This combines 2 approaches. Mulog creates structured events, log lines are
  text. The union of the 2 might just result in a mess.
- No MDC or Marker support. PRs welcome though!

** OK, I'll try this thing out, what should I do?

I'm glad you asked! Here's a minimal setup:

#+BEGIN_SRC sh
  clj -Sdeps '{:deps {com.brunobonacci/mulog {:mvn/version "0.3.1"} nonseldiha/slf4j-mulog {:mvn/version "0.3.0"}}}'
#+END_SRC

#+BEGIN_SRC clojure
  Clojure 1.10.1
  user=> (require '[com.brunobonacci.mulog :as u] '[org.slf4j.impl.mulog :as sm])
  nil
  user=> (u/start-publisher! {:type :console})
  #object[com.brunobonacci.mulog.core$start_publisher_BANG_$stop_publisher__637 0xf08fdce "com.brunobonacci.mulog.core$start_publisher_BANG_$stop_publisher__637@f08fdce"]
  user=> (def logger (org.slf4j.LoggerFactory/getLogger "TESTING"))
  #'user/logger
  user=> (.info logger "Hi there, mulog!")
  nil
  {:mulog/trace-id #mulog/flake "4XNgrTdxZomFYW0PvZmasqs_ian59FL7"
   :mulog/timestamp 1596012449583
   :mulog/event-name :log/message
   :mulog/namespace "TESTING"
   :mulog/origin :slf4j-mulog
   :log/message "Hi there, mulog!"
   :log/level :info}

#+END_SRC

By default only =INFO= and higher levels are pushed through to mulog, but you
can customize this behavior:

#+BEGIN_SRC clojure
  user=> (.debug logger "DEBUG")
  nil
  user=> (sm/set-level! :debug)
  #object[org.slf4j.event.Level 0x25cd49a4 "DEBUG"]
  user=> (.debug logger "DEBUG")
  nil
  {:mulog/trace-id #mulog/flake "4XNh0TksRQIUJwy_PTkWNGKuvmWsJLug"
   :mulog/timestamp 1596012621411
   :mulog/event-name :log/message
   :mulog/namespace "TESTING"
   :mulog/origin :slf4j-mulog
   :log/message "DEBUG"
   :log/level :debug}

#+END_SRC

Events can be further filtered on mulog's side too.

** When should I use this?

As stated in the beginning there are pros and cons to unifying these 2 worlds
and in the end you'll need to make the choice based on your use case, situation,
architecture etc.

If you decide to use this library we recommend to start parsing the log lines in
a just-in-time manner, i.e. add a parsing rule when you realize you'd get
reasonable leverage from it.

Example: if logger Foo logs a line "Finished processing 2000 requests" you can
enrich your log event:

#+BEGIN_SRC clojure
  (defn foo-processed [event]
    (if (and (= "Foo" (:mulog/namespace event))
             (clojure.string/starts-with? "Finished processing " (:log/message event)))
      (assoc event :mulog/event-name :Foo/processed :count (first (re-find #"\d+" (:log/message event))))
      event))
#+END_SRC

This way you can gradually gain more leverage from the log messages.

It's possible this enrichment technique will become a library one day.
Naturally, if you are already doing something similar through logstash, kafka
etc. there's no reason to duplicate the effort here. Parsing inside your
application can also cause unwanted cpu/memory overhead.

** What does the author of =mulog= think about this library?

Please see our [[https://github.com/BrunoBonacci/mulog/issues/25][related discussion]].

** Build/deploy information

Run the following for documentation:

#+BEGIN_SRC bash
clojure -A:deps -T:build help/doc
#+END_SRC
